package com.nir.tinder.Data.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.nir.tinder.Model.Alarm;

import java.util.List;

@Dao
public interface AlarmDao {
    @Insert
    void addAlarm(Alarm alarm);

    @Update
    void updateAlarm(Alarm alarm);

    @Delete
    void removeAlarm(Alarm alarm);

    @Query("DELETE FROM alarms")
    void removeAll();

    @Insert
    void addAll(List<Alarm> alarms);

    @Query("SELECT * FROM alarms ORDER BY date DESC")
    LiveData<List<Alarm>> getAllAlarms();

    @Query("SELECT * FROM alarms WHERE name = :name")
    LiveData<List<Alarm>> getAllByName(String name);
}
