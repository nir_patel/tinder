package com.nir.tinder.Data.DB;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.nir.tinder.Data.DAO.AlarmDao;
import com.nir.tinder.Model.Alarm;
import com.nir.tinder.R;

@Database(entities = {Alarm.class}, version = 3, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;

    public static synchronized AppDatabase getInstance() {
        return instance;
    }

    public abstract AlarmDao alarmDao();

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE `alarms`" +
                             "ADD `notificationId` INTEGER DEFAULT 1 NOT NULL");
        }
    };

    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("DROP TABLE `alarms`");
            database.execSQL("CREATE TABLE `alarms` (" +
                             "`id` INTEGER PRIMARY KEY NOT NULL," +
                             "`name` TEXT," +
                             "`description` TEXT," +
                             "`date` INTEGER)");
        }
    };

    public static void initDatabase(Context context) {
        instance = Room.databaseBuilder(context.getApplicationContext(),
                                        AppDatabase.class,
                                        context.getString(R.string.db_name))
                        .addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                        .build();
    }
}
