package com.nir.tinder.Data.DB;

import androidx.room.TypeConverter;

import java.util.Date;

public class DateConverter {

    @TypeConverter
    public static Date toDate(Long dateLong) {
        if (dateLong != null) {
            return new Date(dateLong);
        } else {
            return null;
        }
    }

    @TypeConverter
    public static Long fromDate(Date date) {
        if (date != null) {
            return date.getTime();
        } else {
            return null;
        }
    }


}
