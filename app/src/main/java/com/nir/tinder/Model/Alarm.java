package com.nir.tinder.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.nir.tinder.Data.DB.DateConverter;
import com.nir.tinder.Repository.DateHandler;

import java.io.IOException;
import java.util.Date;

@Entity(tableName = "alarms")
@TypeConverters(DateConverter.class)
public class Alarm {

    @PrimaryKey
    @ColumnInfo(name = "id")
    public int alarmId;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "description")
    public String description;

    @JsonDeserialize(using = DateHandler.class)
    @ColumnInfo(name = "date")
    public Date date;

    public Alarm() {}

    @Ignore
    public Alarm(int id, String name, String description, Date date) {
        this.alarmId = id;
        this.name = name;
        this.description = description;
        this.date = date;
    }

    @Ignore
    public Alarm(String name, String description, Date date) {
        this.name = name;
        this.description = description;
        this.date = date;
    }

    public Alarm getCopy(Alarm source) {
        final Alarm copy = new Alarm(source.name, source.description, source.date);
        copy.alarmId = this.alarmId;
        return copy;
    }

    public String toJson() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Alarm fromJson(String alarmJson) {
        try {
            return new ObjectMapper().readValue(alarmJson, Alarm.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}