package com.nir.tinder.Repository;

import com.nir.tinder.Model.Alarm;

public interface AlarmDataSet {

    void addAlarm(Alarm alarm);

    void updateAlarm(Alarm alarm);

    void removeAlarm(Alarm alarm);
}
