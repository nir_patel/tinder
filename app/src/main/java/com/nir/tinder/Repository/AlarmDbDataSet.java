package com.nir.tinder.Repository;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.nir.tinder.Data.DAO.AlarmDao;
import com.nir.tinder.Data.DB.AppDatabase;
import com.nir.tinder.Model.Alarm;

import java.util.List;

import javax.inject.Inject;

public class AlarmDbDataSet implements AlarmLocalDataSet {

    private AlarmDao alarmDao;

    @Inject
    public AlarmDbDataSet() {
        alarmDao = AppDatabase.getInstance().alarmDao();
    }

    private static class AddAlarmAsyncTask extends AsyncTask<Alarm, Void, Void> {
        private AlarmDao alarmDao;

        protected AddAlarmAsyncTask(AlarmDao dao) {
            this.alarmDao = dao;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            alarmDao.addAlarm(alarms[0]);
            return null;
        }
    }

    @Override
    public void addAlarm(Alarm alarm) {
        new AddAlarmAsyncTask(alarmDao).execute(alarm);
    }

    private static class UpdateAlarmAsyncTask extends AsyncTask<Alarm, Void, Void> {
        private AlarmDao alarmDao;

        private UpdateAlarmAsyncTask(AlarmDao dao) {
            this.alarmDao = dao;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            alarmDao.updateAlarm(alarms[0]);
            return null;
        }
    }

    @Override
    public void updateAlarm(Alarm alarm) {
        new UpdateAlarmAsyncTask(alarmDao).execute(alarm);
    }

    private static class RemoveAlarmAsyncTask extends AsyncTask<Alarm, Void, Void> {
        private AlarmDao alarmDao;

        private RemoveAlarmAsyncTask(AlarmDao dao) {
            this.alarmDao = dao;
        }

        @Override
        protected Void doInBackground(Alarm... alarms) {
            alarmDao.removeAlarm(alarms[0]);
            return null;
        }
    }

    @Override
    public void removeAlarm(Alarm alarm) {
        new RemoveAlarmAsyncTask(alarmDao).execute(alarm);
    }

    private static class RemoveAllAsyncTask extends  AsyncTask<Void, Void, Void> {
        private AlarmDao alarmDao;

        private RemoveAllAsyncTask(AlarmDao dao) {
            this.alarmDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            alarmDao.removeAll();
            return null;
        }
    }

    @Override
    public void removeAll() {
        new RemoveAllAsyncTask(alarmDao).execute();
    }

    @Override
    public LiveData<List<Alarm>> getAllAlarms() {
        return alarmDao.getAllAlarms();
    }

    private static class AddAllAsyncTask extends AsyncTask<List<Alarm>, Void, Void> {
        private AlarmDao alarmDao;

        protected AddAllAsyncTask(AlarmDao dao) {
            this.alarmDao = dao;
        }

        @Override
        protected Void doInBackground(List<Alarm>... lists) {
            alarmDao.addAll(lists[0]);
            return null;
        }
    }

    @Override
    public void addAll(List<Alarm> alarms) {
        new AddAllAsyncTask(alarmDao).execute(alarms);
    }
}
