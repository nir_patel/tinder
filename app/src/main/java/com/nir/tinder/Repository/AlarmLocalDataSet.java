package com.nir.tinder.Repository;

import androidx.lifecycle.LiveData;

import com.nir.tinder.Model.Alarm;

import java.util.List;

public interface AlarmLocalDataSet extends AlarmDataSet {
    LiveData<List<Alarm>> getAllAlarms();

    void addAll(List<Alarm> alarms);

    void removeAll();

}
