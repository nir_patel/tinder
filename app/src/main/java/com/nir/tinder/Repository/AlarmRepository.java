package com.nir.tinder.Repository;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.nir.tinder.Model.Alarm;

import java.util.List;

import javax.inject.Inject;

public class AlarmRepository implements RemoteInfoListener {

    private AlarmLocalDataSet dbAccess;
    private AlarmRemoteDataSet serverAccess;

    @Inject
    public AlarmRepository(AlarmDbDataSet dbDataSet) {
        this.dbAccess = dbDataSet;
        this.serverAccess = new AlarmServerDataSet(this);
    }

    public void addAlarm(final Alarm alarm) {
        Log.i("AlarmRepository", "dagger success");
        dbAccess.addAlarm(alarm);
        serverAccess.addAlarm(alarm);
    }

    public void updateAlarm(Alarm alarm) {
        dbAccess.updateAlarm(alarm);
        serverAccess.updateAlarm(alarm);
    }

    public void removeAlarm(Alarm alarm) {
        dbAccess.removeAlarm(alarm);
        serverAccess.removeAlarm(alarm);
    }

    public LiveData<List<Alarm>> getAllFromDB() {
        return dbAccess.getAllAlarms();
    }

    public void selectAllFromServer() {
        serverAccess.selectAlarmList();
    }

    @Override
    public void onAlarmListAccepted(List<Alarm> alarmList) {
        List<ServerListener> serverListenerList = ServerListenersProvider.getServerListeners();
        for (ServerListener serverListener : serverListenerList) {
            serverListener.onAlarmListAccepted(alarmList);
        }
    }
}
