package com.nir.tinder.Repository;

import com.nir.tinder.ViewModel.AlarmViewModel;

import dagger.Component;

@Component
public interface AlarmRepositoryComponent {

    void inject(AlarmViewModel alarmViewModel);
}
