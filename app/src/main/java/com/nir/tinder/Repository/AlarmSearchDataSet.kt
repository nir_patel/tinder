package com.nir.tinder.Repository

import androidx.lifecycle.LiveData
import com.nir.tinder.Data.DAO.AlarmDao
import com.nir.tinder.Model.Alarm

interface AlarmSearchDataSet {

    val alarmDao : AlarmDao

    suspend fun getAllByName(name : String) : LiveData<List<Alarm>>
}