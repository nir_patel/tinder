package com.nir.tinder.Repository

import androidx.lifecycle.LiveData
import com.nir.tinder.Data.DAO.AlarmDao
import com.nir.tinder.Data.DB.AppDatabase
import com.nir.tinder.Model.Alarm

class AlarmSearchDbDataSet : AlarmSearchDataSet {

    override val alarmDao: AlarmDao = AppDatabase.getInstance().alarmDao()

    override suspend fun getAllByName(name: String): LiveData<List<Alarm>> {
        return alarmDao.getAllByName(name)
    }

}