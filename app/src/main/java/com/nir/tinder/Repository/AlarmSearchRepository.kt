package com.nir.tinder.Repository

import androidx.lifecycle.LiveData
import com.nir.tinder.Model.Alarm

object AlarmSearchRepository {

    private val dataSet : AlarmSearchDataSet = AlarmSearchDbDataSet()

    suspend fun getAllByName(name : String) : LiveData<List<Alarm>>
        = dataSet.getAllByName(name)
}