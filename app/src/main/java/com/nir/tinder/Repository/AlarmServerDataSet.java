package com.nir.tinder.Repository;

import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.nir.tinder.Model.Alarm;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AlarmServerDataSet implements AlarmRemoteDataSet {

    private RemoteInfoListener remoteInfoListener;

    public AlarmServerDataSet(RemoteInfoListener listener) {
        this.remoteInfoListener = listener;
    }

    @Override
    public void addAlarm(Alarm alarm) {
        AlarmSocketService.emit("add to-server", alarm.toJson());
    }

    @Override
    public void updateAlarm(Alarm alarm) {
        AlarmSocketService.emit("update to-server", alarm.toJson());
    }

    @Override
    public void removeAlarm(Alarm alarm) {
        AlarmSocketService.emit("remove to-server", alarm.toJson());
    }

    @Override
    public void selectAlarmList() {
        final List<Alarm> alarmList = new ArrayList<>();

        AlarmSocketService.initSocket();
        AlarmSocketService.emit("get all from-server");
        AlarmSocketService.on("send all to-client", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("Event", "Accepted alarm list from server");
                String jsonList = (String) args[0];
                JSONArray jsonArray = new JSONArray();

                try {
                    jsonArray = new JSONArray(jsonList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                List<String> stringList = new ArrayList<>();

                for (int arrIndex = 0; arrIndex < jsonArray.length(); arrIndex++) {
                    try {
                        stringList.add(jsonArray.getString(arrIndex));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                for (String alarmJson : stringList) {
                    alarmList.add(Alarm.fromJson(alarmJson));
                }


                remoteInfoListener.onAlarmListAccepted(alarmList);
            }
        });
    }
}
