package com.nir.tinder.Repository;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class AlarmSocketService extends Service {

    private static final String ADDRESS = "http://10.0.2.2";
    private static final int PORT = 3000;

    private static Socket socket;

    @Override
    public void onCreate() {
        super.onCreate();
        setServerListener();
        socket.connect();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Visit", "onStartCommand");

        if (socket == null) {
            initSocket();
        }

        setServerListener();
        return START_STICKY_COMPATIBILITY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName()); // MIRI

        final int REQUEST_CODE = 1;
        PendingIntent pendingIntent =
                PendingIntent.getService(getApplicationContext(),
                                         REQUEST_CODE,
                                         restartServiceIntent,
                                         PendingIntent.FLAG_ONE_SHOT);

        AlarmManager alarmManager =
                (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME,
                         SystemClock.elapsedRealtime() + 10,
                         pendingIntent);

        super.onTaskRemoved(rootIntent);
    }

    public void setServerListener() {
        initSocket();
        ServerListenersProvider.setEvents(this);
    }

    public static void initSocket() {
        try {
            socket = IO.socket(ADDRESS + ":" + PORT);
        } catch (URISyntaxException e) {
            Log.i("Nir", "URISyntaxException");
        }
    }

    public static void on(String eventName, Emitter.Listener listener) {
        socket.on(eventName, listener);
    }

    public static void off(String eventName) {
        socket.off(eventName);
    }

    public static void emit(String eventName, Object... args) {
        socket.emit(eventName, args);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
