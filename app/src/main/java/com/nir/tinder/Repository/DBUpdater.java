package com.nir.tinder.Repository;

import android.util.Log;

import com.nir.tinder.Model.Alarm;

import java.util.List;

public class DBUpdater implements ServerListener {

    private AlarmLocalDataSet dbAccess;

    public DBUpdater(AlarmLocalDataSet dbAccess) {
        this.dbAccess = dbAccess;
    }

    @Override
    public void onAlarmAdded(Alarm alarm) {
        dbAccess.addAlarm(alarm);
    }

    @Override
    public void onAlarmUpdated(Alarm alarm) {
        dbAccess.updateAlarm(alarm);
    }

    @Override
    public void onAlarmRemoved(Alarm alarm) {
        dbAccess.removeAlarm(alarm);
    }

    @Override
    public void onAlarmListAccepted(List<Alarm> alarmList) {
        Log.i("Nir", "start updating DB");
        dbAccess.removeAll();
        dbAccess.addAll(alarmList);
    }
}
