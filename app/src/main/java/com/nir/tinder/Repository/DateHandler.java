package com.nir.tinder.Repository;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.nir.tinder.Data.DB.DateConverter;

import java.io.IOException;
import java.util.Date;

public class DateHandler extends StdDeserializer<Date> {

    @SuppressWarnings("unused")
    public DateHandler() {
        this(null);
    }

    public DateHandler(Class<?> matchClass) {
        super(matchClass);
    }

    @Override
    public Date deserialize(JsonParser p, DeserializationContext context)
            throws IOException {
        return DateConverter.toDate(p.getValueAsLong());
    }
}
