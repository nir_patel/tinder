package com.nir.tinder.Repository;

import android.util.Log;

import androidx.core.util.Consumer;

import com.github.nkzawa.emitter.Emitter;
import com.nir.tinder.Model.Alarm;

import java.util.List;

public class EventSetter implements Consumer<List<ServerListener>> {

    @Override
    public void accept(final List<ServerListener> listeners) {
        AlarmSocketService.off("add to-client");
        AlarmSocketService.on("add to-client", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i("Visit", "add to-client event emitted");
                Alarm alarmToAdd = Alarm.fromJson((String) args[0]);
                for (ServerListener listener : listeners) {
                    listener.onAlarmAdded(alarmToAdd);
                }
            }
        });
        AlarmSocketService.off("update to-client");
        AlarmSocketService.on("update to-client", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Alarm updated = Alarm.fromJson((String) args[0]);
                for (ServerListener listener : listeners) {
                    listener.onAlarmUpdated(updated);
                }
            }
        });
        AlarmSocketService.off("remove to-client");
        AlarmSocketService.on("remove to-client", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Alarm alarmToRemove = Alarm.fromJson((String) args[0]);
                for (ServerListener listener : listeners) {
                    listener.onAlarmRemoved(alarmToRemove);
                }
            }
        });
    }
}
