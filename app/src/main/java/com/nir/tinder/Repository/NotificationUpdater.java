package com.nir.tinder.Repository;

import android.content.Context;

import com.nir.tinder.Model.Alarm;
import com.nir.tinder.View.AlarmNotification;

import java.lang.ref.WeakReference;
import java.util.List;

public class NotificationUpdater implements ServerListener {

    private static NotificationUpdater instance;
    private WeakReference<Context> contextWeakReference;

    private NotificationUpdater(Context activity) {
        this.contextWeakReference = new WeakReference<>(activity);
    }

    public static NotificationUpdater getInstance(Context context) {
        if (instance == null) {
            instance = new NotificationUpdater(context);
        }

        return instance;
    }

    @Override
    public void onAlarmAdded(Alarm alarm) {
        AlarmNotification alarmNotification =
                new AlarmNotification(contextWeakReference.get(), alarm);
        alarmNotification.configure();
    }

    @Override
    public void onAlarmUpdated(Alarm alarm) {
        AlarmNotification alarmNotification =
                new AlarmNotification(contextWeakReference.get(), alarm);
        alarmNotification.configure();
    }

    @Override
    public void onAlarmRemoved(Alarm alarm) {
        AlarmNotification alarmNotification =
                new AlarmNotification(contextWeakReference.get(), alarm);
        alarmNotification.cancel();
    }

    @Override
    public void onAlarmListAccepted(List<Alarm> alarmList) {
        for (Alarm alarm : alarmList) {
            this.onAlarmAdded(alarm);
        }
    }
}
