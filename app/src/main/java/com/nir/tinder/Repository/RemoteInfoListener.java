package com.nir.tinder.Repository;

import com.nir.tinder.Model.Alarm;

import java.util.List;

public interface RemoteInfoListener {
    void onAlarmListAccepted(List<Alarm> alarmList);
}
