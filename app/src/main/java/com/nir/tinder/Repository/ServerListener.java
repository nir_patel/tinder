package com.nir.tinder.Repository;

import com.nir.tinder.Model.Alarm;

import java.util.List;

public interface ServerListener {

    void onAlarmAdded(Alarm alarm);

    void onAlarmUpdated(Alarm alarm);

    void onAlarmRemoved(Alarm alarm);

    void onAlarmListAccepted(List<Alarm> alarmList);
}
