package com.nir.tinder.Repository;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

public class ServerListenersProvider {

    private static List<ServerListener> serverListeners = new ArrayList<>();

    public static void setEvents(Context context) {
        populateList(context);
        (new EventSetter()).accept(serverListeners);
    }

    private static void populateList(Context context) {
        if (serverListeners.isEmpty()) {
            AlarmDbDataSet dbDataSet = new AlarmDbDataSet();
            serverListeners.add(new DBUpdater(dbDataSet));
            serverListeners.add(NotificationUpdater.getInstance(context));
        }
    }

    public static List<ServerListener> getServerListeners() {
        return serverListeners;
    }
}
