package com.nir.tinder.View;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nir.tinder.Model.Alarm;
import com.nir.tinder.R;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.List;

public class AlarmAdapter extends RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder> {

    private AlarmDetailsOpener detailsOpener;

    private View listItem;
    private WeakReference<Context> contextWeakReference;
    private List<Alarm> alarmSet;

    public class AlarmViewHolder extends RecyclerView.ViewHolder {

        public TextView alarmName;
        public TextView alarmDesc;
        public TextView alarmTime;
        public TextView alarmDate;
        public TextView alarmDayOfWeek;

        public AlarmViewHolder(View alertView) {
            super(alertView);
            this.alarmName = alertView.findViewById(R.id.alertNameDisplay);
            this.alarmDesc = alertView.findViewById(R.id.alertDescDisplay);
            this.alarmTime = alertView.findViewById(R.id.alertTimeDisplay);
            this.alarmDate = alertView.findViewById(R.id.alertDateDisplay);
            this.alarmDayOfWeek = alertView.findViewById(R.id.alertDayOfWeekDisplay);
        }
    }

    public AlarmAdapter(AlarmDetailsOpener detailsOpener, Context context, List<Alarm> alarmSet) {
        this.detailsOpener = detailsOpener;
        this.contextWeakReference = new WeakReference<>(context);
        this.alarmSet = alarmSet;
    }

    @NonNull
    @Override
    public AlarmViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        listItem = layoutInflater.inflate(R.layout.alarms_list_item,
                                          parent,
                               false);
        return (new AlarmViewHolder(listItem));
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmViewHolder holder, final int position) {
        listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                detailsOpener.openAlarmEditWindow(alarmSet.get(position));
            }
        });

        initHolder(holder, position);
    }

    private void initHolder(AlarmViewHolder holder, final int position) {
        if (alarmSet.get(position).name.equals("")) {
            holder.alarmName.setText(contextWeakReference.get().getString(R.string.alarm_empty_name));
        } else {
            holder.alarmName.setText(alarmSet.get(position).name);
        }

        if (alarmSet.get(position).description.equals("")) {
            holder.alarmDesc.setText(contextWeakReference.get().getString(R.string.alarm_empty_desc));
        } else {
            holder.alarmDesc.setText(alarmSet.get(position).description);
        }

        SimpleDateFormat alarmTimeFormat = ((SimpleDateFormat) SimpleDateFormat.getDateInstance());
        alarmTimeFormat.applyPattern(contextWeakReference.get().getString(R.string.time_display_format));
        holder.alarmTime.setText(alarmTimeFormat.format(alarmSet.get(position).date));

        SimpleDateFormat alarmDateFormat = ((SimpleDateFormat) SimpleDateFormat.getDateInstance());
        alarmDateFormat.applyPattern(contextWeakReference.get().getString(R.string.date_display_format));
        holder.alarmDate.setText(alarmDateFormat.format(alarmSet.get(position).date));

        SimpleDateFormat alarmDayOfWeekFormat =
                ((SimpleDateFormat) SimpleDateFormat.getDateInstance());
        alarmDayOfWeekFormat
                .applyPattern(contextWeakReference.get().getString(R.string.day_of_week_display_format));
        holder.alarmDayOfWeek.setText(alarmDayOfWeekFormat.format(alarmSet.get(position).date));
    }

    @Override
    public int getItemCount() {
        return alarmSet.size();
    }

    public void setData(List<Alarm> updatedList) {
        this.alarmSet = updatedList;
    }
}