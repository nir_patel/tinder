package com.nir.tinder.View;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.nir.tinder.Model.Alarm;
import com.nir.tinder.R;
import com.nir.tinder.ViewModel.AlarmDetailsViewModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class AlarmDetailsFragment extends Fragment {

    private Alarm currAlarm;
    private AlarmDetailsViewModel viewModel;

    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat dateFormat;
    private TimePicker timePicker;

    public AlarmDetailsFragment(Alarm currAlarm) {
        super();
        this.currAlarm = currAlarm;
    }

    public AlarmDetailsFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        viewModel = ViewModelProviders.of(this).get(AlarmDetailsViewModel.class);

        ActionBar actionBar = ((MainActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initCurrValue();

        final DatePickerDialog.OnDateSetListener pickedDate =
                new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(Calendar.YEAR ,year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                TextView dateTextView = getView().findViewById(R.id.pickedDateLabel);
                dateTextView.setText(dateFormat.format(calendar.getTime()));
            }
        };

        Button pickDateButton = view.findViewById(R.id.pickDateButton);
        pickDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog =
                        new DatePickerDialog(getContext(),
                                             pickedDate,
                                             calendar.get(Calendar.YEAR),
                                             calendar.get(Calendar.MONTH),
                                             calendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();

            }
        });

    }

    private void initCurrValue() {
        String title;
        EditText alarmNameText = getView().findViewById(R.id.alarmNameInput);
        EditText alarmDescText = getView().findViewById(R.id.alarmDescInput);
        final TextView alarmDateText = getView().findViewById(R.id.pickedDateLabel);
        dateFormat = new SimpleDateFormat(getString(R.string.date_format));
        timePicker = getView().findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);

        if (currAlarm == null) {
            title = getString(R.string.new_alert_title);

            Date currDate = Calendar.getInstance().getTime();
            alarmDateText.setText(dateFormat.format(currDate));
        } else {
            alarmNameText.setText(currAlarm.name);
            alarmNameText.setSelection(alarmNameText.getText().length());

            alarmDescText.setText(currAlarm.description);
            alarmDescText.setSelection(alarmDescText.getText().length());

            alarmDateText.setText(dateFormat.format(currAlarm.date));

            calendar.setTime(currAlarm.date);

            timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
            timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));

            title = getString(R.string.edit_alert_title);
        }

        getActivity().setTitle(title);

        alarmNameText.setFocusableInTouchMode(true);
        alarmNameText.requestFocus();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        int menuId;

        if (currAlarm == null) {
            menuId = R.menu.actions_bar_details_activity;
        } else {
            menuId = R.menu.actions_bar_edition_activity;
        }

        inflater.inflate(menuId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newAlertAdd: {
                Alarm newAlarm = createAlarm((EditText) getView().findViewById(R.id.alarmNameInput),
                                             (EditText) getView().findViewById(R.id.alarmDescInput),
                                             timePicker);
                newAlarm.alarmId = generateAlarmId(getActivity());

                if (newAlarm.date.getTime() <= System.currentTimeMillis()) {
                    Toast
                            .makeText(getActivity(),
                                    getContext().getString(R.string.past_date_error),
                                    Toast.LENGTH_SHORT)
                            .show();
                } else {
                    viewModel.addAlarm(newAlarm);

                    AlarmNotification alarmNotification =
                            new AlarmNotification(getContext(), newAlarm);
                    alarmNotification.configure();

                    getActivity().onBackPressed();
                }
                break;
            }
            case R.id.editionSave: {
                Alarm updated = createAlarm((EditText) getView().findViewById(R.id.alarmNameInput),
                                            (EditText) getView().findViewById(R.id.alarmDescInput),
                                            timePicker);

                if (updated.date.getTime() <= System.currentTimeMillis()) {
                    Toast
                      .makeText(getActivity(),
                                getContext().getString(R.string.past_date_error),
                                Toast.LENGTH_SHORT)
                      .show();
                }
                else {
                    viewModel.updateAlarm(currAlarm, updated);

                    AlarmNotification alarmNotification =
                            new AlarmNotification(getContext(), currAlarm.getCopy(updated));
                    alarmNotification.configure();

                    getActivity().onBackPressed();
                }
                break;
            }
            case android.R.id.home: {
                getActivity().onBackPressed();
            }
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private Alarm createAlarm(EditText alarmNameView,
                              EditText alarmDescView,
                              TimePicker dateTime) {
        String newAlarmName = alarmNameView.getText().toString().trim();
        String newAlarmDesc = alarmDescView.getText().toString().trim();

        calendar.set(Calendar.HOUR_OF_DAY, dateTime.getCurrentHour());
        calendar.set(Calendar.MINUTE, dateTime.getCurrentMinute());
        calendar.set(Calendar.SECOND, 0);
        Date newAlarmDate = calendar.getTime();

        Alarm created = new Alarm(newAlarmName, newAlarmDesc, newAlarmDate);

        return created;
    }

    public int generateAlarmId(Context context) {
        SharedPreferences sharedPreferences =
                context.getSharedPreferences(context.getString(R.string.notification_settings),
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        final String idKey = context.getString(R.string.notification_id);

        if (!sharedPreferences.contains(idKey)) {
            editor.putInt(idKey, 1);
            editor.apply();
        }

        int idToReturn = sharedPreferences.getInt(idKey, 0);
        editor.putInt(idKey, idToReturn + 1);
        editor.apply();

        return idToReturn;
    }
}
