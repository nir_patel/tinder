package com.nir.tinder.View;

import com.nir.tinder.Model.Alarm;

public interface AlarmDetailsOpener {

    void openAlarmEditWindow(Alarm alarm);

    void openAlarmAdditionWindow();
}
