package com.nir.tinder.View;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import androidx.core.app.NotificationCompat;

import com.nir.tinder.Model.Alarm;
import com.nir.tinder.R;

import java.lang.ref.WeakReference;

public class AlarmNotification {

    private WeakReference<Context> contextWeakReference;
    private Alarm alarm;
    private Notification notification;
    private PendingIntent operation;
    private AlarmManager alarmManager;

    public AlarmNotification(Context context, Alarm alarm) {
        this.contextWeakReference = new WeakReference<>(context);
        this.alarm = alarm;
        this.alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    private PendingIntent getResultPendingIntent() {
        Intent resultIntent =
                new Intent(contextWeakReference.get(), contextWeakReference.get().getClass());

        return PendingIntent.getActivity(contextWeakReference.get(),
                                         alarm.alarmId,
                                         resultIntent,
                                         PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void setNotification() {
        Notification.Builder builder = new Notification.Builder(contextWeakReference.get())
                .setSmallIcon(R.drawable.rounded_button)
                .setContentTitle(alarm.name)
                .setContentText(alarm.description)
                .setContentIntent(getResultPendingIntent())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);
        notification = builder.build();
    }

    private void createOperation() {
        setNotification();
        Intent intent = new Intent(contextWeakReference.get(), AlarmNotificationReceiver.class);
        intent.putExtra(AlarmNotificationReceiver.ID_KEY, (int) System.currentTimeMillis());
        intent.putExtra(AlarmNotificationReceiver.NOTIFICATION_KEY, notification);

        operation =
                PendingIntent.getBroadcast(contextWeakReference.get(),
                                           alarm.alarmId,
                                           intent,
                                           PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void configure() {
        createOperation();
        long systemRealTime = SystemClock.elapsedRealtime();
        long alarmTime = alarm.date.getTime();
        long notificationTime = systemRealTime + (alarmTime - System.currentTimeMillis());

        if (notificationTime > systemRealTime) {
            alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, notificationTime, operation);
        }
    }

    private void findOperation() {
        Intent intent = new Intent(contextWeakReference.get(), AlarmNotificationReceiver.class);

        operation = PendingIntent.getBroadcast(contextWeakReference.get(),
                                               alarm.alarmId,
                                               intent,
                                               PendingIntent.FLAG_NO_CREATE);
    }

    public void cancel() {
        findOperation();

        if ((alarm.date.getTime() > System.currentTimeMillis()) &&
            (operation != null)) {
            alarmManager.cancel(operation);
        }
    }
}
