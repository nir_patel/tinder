package com.nir.tinder.View;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmNotificationReceiver extends BroadcastReceiver {

    public static final String ID_KEY = "notification_id";
    public static final String NOTIFICATION_KEY = "notification";

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationManager manager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int id = intent.getIntExtra(ID_KEY, 0);
        Notification notification = intent.getParcelableExtra(NOTIFICATION_KEY);

        manager.notify(id, notification);
    }
}
