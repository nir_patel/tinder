package com.nir.tinder.View;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nir.tinder.Model.Alarm;
import com.nir.tinder.R;
import com.nir.tinder.ViewModel.AlarmListViewModel;

import java.util.ArrayList;
import java.util.List;

public class AlarmsFragment extends Fragment {

    private AlarmDetailsOpener alarmDetailsOpener;

    private AlarmListViewModel viewModel;
    private List<Alarm> alarmList;

    public AlarmsFragment(AlarmDetailsOpener detailsOpener) {
        super();
        this.alarmDetailsOpener = detailsOpener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        viewModel = ViewModelProviders.of(this).get(AlarmListViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton newAlertButton = view.findViewById(R.id.addNewAlertButton);
        newAlertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmDetailsOpener.openAlarmAdditionWindow();
            }
        });

        NotificationManager notificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    ItemTouchHelper.SimpleCallback alarmCallback =
            new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView,
                                      @NonNull RecyclerView.ViewHolder viewHolder,
                                      @NonNull RecyclerView.ViewHolder target) {
                    return false;
                }

                @Override
                public void onSwiped(@NonNull final RecyclerView.ViewHolder viewHolder,
                                                                        int direction) {
                    final int position = viewHolder.getAdapterPosition();
                    final RecyclerView recyclerView =
                            getView().findViewById(R.id.alarmsRecyclerView);
                    recyclerView.getAdapter().notifyDataSetChanged();
                    DialogInterface.OnClickListener dialogClickListener =
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_POSITIVE) {
                                        removeAlarm(recyclerView, position);
                                        updateNoAlarmsMessageVisibility();
                                    }
                                }
                            };

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setMessage(getString(R.string.remove_alarm_confirm_message))
                            .setPositiveButton(getString(R.string.remove_alarm_positive),
                                    dialogClickListener)
                            .setNegativeButton(getString(R.string.remove_alarm_negative),
                                    dialogClickListener)
                            .show();
                }
            };

    private void removeAlarm(RecyclerView recyclerView, int alarmPosition) {
        Alarm alarmToRemove = alarmList.get(alarmPosition);
        viewModel.removeAlarm(alarmToRemove);
        recyclerView.getAdapter().notifyItemRemoved(alarmPosition);

        AlarmNotification alarmNotification =
                new AlarmNotification(getContext(), alarmToRemove);
        alarmNotification.cancel();
    }

    private void updateNoAlarmsMessageVisibility() {
        int visibility;

        if (alarmList.isEmpty()) {
            visibility = View.VISIBLE;
        } else {
            visibility = View.INVISIBLE;
        }

        getView().findViewById(R.id.noAlarmsMessage).setVisibility(visibility);
    }

    @Override
    public void onStart() {
        super.onStart();

        SharedPreferences userPreferences =
                getActivity().getSharedPreferences(getString(R.string.user_settings),
                                                   Context.MODE_PRIVATE);
        String userNameKey = getString(R.string.user_name_key);
        String title = getString(R.string.list_activity_base_title,
                                 userPreferences.getString(userNameKey, ""));
        getActivity().setTitle(title);

        initRecyclerView();

        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);

        RecyclerView recyclerView = getView().findViewById(R.id.alarmsRecyclerView);
        final AlarmAdapter adapter = (AlarmAdapter) recyclerView.getAdapter();

        viewModel.getAllFromDB().observe(this, new Observer<List<Alarm>>() {
            @Override
            public void onChanged(List<Alarm> alarmList) {
                AlarmsFragment.this.alarmList = alarmList;
                adapter.setData(alarmList);
                adapter.notifyDataSetChanged();
                updateNoAlarmsMessageVisibility();
            }
        });
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = getView().findViewById(R.id.alarmsRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        DividerItemDecoration decoration =
                new DividerItemDecoration(recyclerView.getContext(),
                        DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(decoration);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(alarmCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        final AlarmAdapter adapter =
                new AlarmAdapter(alarmDetailsOpener, getContext(), new ArrayList<Alarm>());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.options_menu_list_activity, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profileOption: {
                Intent activityIntent =
                        new Intent(getContext(), PreferencesActivity.class);
                startActivity(activityIntent);
                break;
            }
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
