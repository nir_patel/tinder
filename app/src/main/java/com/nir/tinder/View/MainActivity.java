package com.nir.tinder.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.nir.tinder.Model.Alarm;
import com.nir.tinder.R;
import com.nir.tinder.Repository.AlarmSocketService;
import com.nir.tinder.ViewModel.AlarmMainViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements AlarmDetailsOpener {

    private FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startService(new Intent(this, AlarmSocketService.class));

        AlarmMainViewModel viewModel =
                ViewModelProviders.of(this).get(AlarmMainViewModel.class);

        final LiveData<List<Alarm>> currListLiveData = viewModel.getAll();
        currListLiveData.observe(this, new Observer<List<Alarm>>() {
            @Override
            public void onChanged(List<Alarm> alarmList) {
                for (Alarm alarm : alarmList) {
                    AlarmNotification alarmNotification =
                            new AlarmNotification(MainActivity.this, alarm);
                    alarmNotification.cancel();
                    currListLiveData.removeObserver(this);
                }
            }
        });
        viewModel.selectAlarmList();

        transact(new AlarmsFragment(this));
    }

    @Override
    protected void onStart() {
        super.onStart();

        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    public void openAlarmEditWindow(Alarm alarm) {
        Fragment fragment = new AlarmDetailsFragment(alarm);
        transact(fragment);
    }

    @Override
    public void openAlarmAdditionWindow() {
        transact(new AlarmDetailsFragment());
    }

    private void transact(Fragment fragment) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_frame, fragment)
                .addToBackStack(null)
                .commit();
    }
}
