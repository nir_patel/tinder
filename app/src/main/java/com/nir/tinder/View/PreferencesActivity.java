package com.nir.tinder.View;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.nir.tinder.R;

public class PreferencesActivity extends AppCompatActivity {

    private EditText userNameText;
    private SharedPreferences userPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        userPreferences = getSharedPreferences(getString(R.string.user_settings),
                                               Context.MODE_PRIVATE);

        setTitle(getString(R.string.preferences_activity_title));

        userNameText = findViewById(R.id.userNameInput);

        if (userPreferences.contains(getString(R.string.user_name_key))) {
            String userNameKey = getString(R.string.user_name_key);
            userNameText.setText(userPreferences.getString(userNameKey, ""));
            userNameText.setSelection(userNameText.getText().length());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.actions_bar_preferences_activity, menu);
        menu.findItem(R.id.preferencesSave).setVisible(false);

        TextWatcher textChangesListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                String userNameKey = getString(R.string.user_name_key);
                boolean isNameChanged =
                        (userPreferences.contains(getString(R.string.user_name_key))) &
                        !(userNameText.getText().toString()
                          .equals(userPreferences.getString(userNameKey, "")));
                boolean isOriginNameEmpty =
                        !userPreferences.contains(getString(R.string.user_name_key));
                menu.findItem(R.id.preferencesSave).setVisible(isNameChanged || isOriginNameEmpty);
            }
        };

        userNameText.addTextChangedListener(textChangesListener);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.preferencesSave: {
                String newUserName = ((EditText) findViewById(R.id.userNameInput))
                                     .getText()
                                     .toString()
                                     .trim();
                SharedPreferences.Editor editor = userPreferences.edit();
                editor.putString(getString(R.string.user_name_key), newUserName);
                editor.apply();

                onBackPressed();
                break;
            }
            default: {
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
