package com.nir.tinder.View;

import android.app.Application;

import com.nir.tinder.Data.DB.AppDatabase;

public class TinderApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        AppDatabase.initDatabase(getApplicationContext());
    }
}
