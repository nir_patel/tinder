package com.nir.tinder.ViewModel;

import com.nir.tinder.Model.Alarm;

public class AlarmDetailsViewModel extends AlarmViewModel {

    public void addAlarm(Alarm alarm) {
        repository().addAlarm(alarm);
    }

    public void updateAlarm(Alarm old, Alarm updated) {
        repository().updateAlarm(old.getCopy(updated));
    }
}
