package com.nir.tinder.ViewModel;

import androidx.lifecycle.LiveData;

import com.nir.tinder.Model.Alarm;

import java.util.List;

public class AlarmListViewModel extends AlarmViewModel {

    public void removeAlarm(Alarm alarm) {
        repository().removeAlarm(alarm);
    }

    public LiveData<List<Alarm>> getAllFromDB() {
        return repository().getAllFromDB();
    }
}
