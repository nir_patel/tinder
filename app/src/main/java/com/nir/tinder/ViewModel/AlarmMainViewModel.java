package com.nir.tinder.ViewModel;

import androidx.lifecycle.LiveData;

import com.nir.tinder.Model.Alarm;

import java.util.List;

public class AlarmMainViewModel extends AlarmViewModel {

    public void selectAlarmList() {
        repository().selectAllFromServer();
    }

    public LiveData<List<Alarm>> getAll() {
        return repository().getAllFromDB();
    }
}
