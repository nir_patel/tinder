package com.nir.tinder.ViewModel;

import androidx.lifecycle.ViewModel;

import com.nir.tinder.Repository.AlarmRepository;
import com.nir.tinder.Repository.AlarmRepositoryComponent;
import com.nir.tinder.Repository.DaggerAlarmRepositoryComponent;

import javax.inject.Inject;

public abstract class AlarmViewModel extends ViewModel {
    @Inject
    AlarmRepository repository;

    public AlarmViewModel() {
        super();
        AlarmRepositoryComponent component = DaggerAlarmRepositoryComponent.create();
        component.inject(this);
    }

    protected AlarmRepository repository() {
        return repository;
    }
}
