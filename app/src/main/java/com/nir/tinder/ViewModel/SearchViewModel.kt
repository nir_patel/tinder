package com.nir.tinder.ViewModel

import androidx.lifecycle.LiveData
import com.nir.tinder.Model.Alarm
import com.nir.tinder.Repository.AlarmSearchRepository
import kotlinx.coroutines.experimental.*

class SearchViewModel {

    fun getAllByName(name: String): LiveData<List<Alarm>> = runBlocking {

        val asyncList = async(CommonPool) {
            AlarmSearchRepository.getAllByName(name)
        }

        asyncList.await()
    }
}